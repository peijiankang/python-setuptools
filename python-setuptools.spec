%bcond_with bootstrap
%bcond_with tests

%global python3_record %{python3_sitelib}/setuptools-%{version}.dist-info/RECORD

%global python_whldir %{_datadir}/python-wheels
%global python_whlname setuptools-%{version}-py3-none-any.whl

Name: python-setuptools
Version: 59.4.0
Release: 4
Summary: Easily build and distribute Python packages

License: MIT and (BSD or ASL 2.0)
URL: https://pypi.python.org/pypi/setuptools
Source0: %{pypi_source setuptools %{version}}
Patch9000: bugfix-eliminate-random-order-in-metadata.patch

BuildArch: noarch

BuildRequires: python3-devel
%if %{with tests}
BuildRequires: python3-pip, python3-mock
BuildRequires: python3-pytest, python3-pytest-fixture-config
BuildRequires: python3-pytest-virtualenv
%endif
%if %{without bootstrap}
BuildRequires: python3-pip, python3-wheel
%endif

Provides: python-distribute = %{version}-%{release}, %{name}-wheel
Obsoletes: python-distribute < 0.6.36-2, %{name}-wheel

%description
Setuptools is a collection of enhancements to the Python distutils that allow
you to more easily build and distribute Python packages, especially ones that
have dependencies on other packages.

This package contains a python wheel of setuptools to use with venv.

%package -n python3-setuptools
Summary: Easily build and distribute Python 3 packages
Conflicts: python-setuptools < %{version}-%{release}
Provides:  python%{python3_pkgversion}dist(setuptools) = %{version}
Provides:  python%{python3_version}dist(setuptools) = %{version}

%description -n python3-setuptools
Setuptools is a collection of enhancements to the Python 3 distutils that allow
you to more easily build and distribute Python 3 packages, especially ones that
have dependencies on other packages.

This package also contains the runtime components of setuptools, necessary to
execute the software that requires pkg_resources.py.

%package_help

%prep
%autosetup -n setuptools-%{version} -p1

find setuptools pkg_resources -name \*.py | xargs sed -i -e '1 {/^#!\//d}'
rm -f setuptools/*.exe
rm setuptools/tests/test_integration.py
chmod -x README.rst

%build
%if %{without bootstrap}
%py3_build_wheel
%else
%py3_build
%endif

%install
%if %{without bootstrap}
%py3_install_wheel %{python_whlname}
sed -i '/\/usr\/bin\/easy_install,/d' %{buildroot}%{python3_record}
rm -rf %{buildroot}%{python3_sitelib}/{setuptools, pkg_resources}/tests
sed -i '/^setuptools\/tests\//d' %{buildroot}%{python3_record}
find %{buildroot}%{python3_sitelib} -name '*.exe' | xargs rm -f

rm -r docs/{conf.py,_*}
mkdir -p %{buildroot}%{python_whldir}
install -p dist/%{python_whlname} -t %{buildroot}%{python_whldir}
%else
%py3_install
%endif

%if %{with tests}
%check
PYTHONDONTWRITEBYTECODE=1 PYTHONPATH=$(pwd) py.test-%{python3_version} --ignore=setuptools/tests/test_virtualenv.py --ignore=pavement.py
%endif

%files
%defattr(-,root,root)
%license LICENSE
%if %{without bootstrap}
%dir %{python_whldir}/
%{python_whldir}/%{python_whlname}
%endif

%files -n python3-setuptools
%defattr(-,root,root)
%{python3_sitelib}/pkg_resources/
%{python3_sitelib}/setuptools*/
%{python3_sitelib}/distutils-precedence.pth
%{python3_sitelib}/_distutils_hack/

%files help
%defattr(-,root,root)
%doc docs/* CHANGES.rst README.rst


%changelog
* Fri Dec 17 2021 shixuantong<shixuantong@huawei.com> - 59.4.0-4
- Bootstrap for Python 3.10

* Fri Dec 03 2021 shixuantong<shixuantong@huawei.com> - 59.4.0-3
- Provide pythonXdist(setuptools) when bootstrapping

* Wed Dec 01 2021 shixuantong<shixuantong@huawei.com> - 59.4.0-2
- Bootstrap for Python 3.10

* Wed Dec 01 2021 shixuantong<shixuantong@huawei.com> - 59.4.0-1
- update version to 59.4.0

* Thu Nov 25 2021 shixuantong<shixuantong@huawei.com> - 54.2.0-2
- add bootstrap for rebuild python3-3.10

* Mon Mar 29 2021 shixuantong<shixuantong@huawei.com> - 54.2.0-1
- upgrade version to 54.2.0

* Mon Nov 2 2020 wangjie<wangjie294@huawei.com> -44.1.1-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:remove python2

* Wed Sep 2 2020 shixuantong<shixuantong@huawei.com> - 44.1.1 - 1
- update version to 44.1.1

* Tue Aug 25 2020 wenzhanli<wenzhanli2@huawei.com> - 41.5.0-1
- update version and add python2 require

* Thu Jun 4 2020 chengzihan <chengzihan2@huawei.com> - 41.2.0-3
- bugfix eliminate random order in metadata

* Thu Jun 4 2020 chengzihan <chengzihan2@huawei.com> - 41.2.0-2
- Upgrade package to version 41.2.0

* Wed Oct 30 2019 hexiaowen <hexiaowen@huawei.com> - 40.4.3-4
- disable tests

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 40.4.3-3
- Fix python-setuptools-wheel dependency problems

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 40.4.3-2
- Package init
